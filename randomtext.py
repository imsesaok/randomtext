import random
import os

letter_stat=dict()
collocation_stat=dict()
letter_stat_total = 0
collocation_stat_total = 0

def import_stat(stat_dict, filename):
    stat_total = 0
    with open(filename) as stat_file:
        for line in stat_file:
            keyvalue = line.split(',')
            key = keyvalue[0]
            value = int(keyvalue[1])

            stat_dict[key] = value
            stat_total += value

    return stat_total

filepath = os.path.join(os.path.dirname(__file__), 'collocation_stat.txt')
collocation_stat_total = import_stat(collocation_stat, filepath)


def weighted_random(stat_dict, stat_total):
    randint = random.randint(1, stat_total)
    for key, value in stat_dict.items():
        randint -= value
        if randint <= 0:
            return key

def gen_subdict(stat_dict, letter):
    subdict = {key:value for key,value in stat_dict.items() if key[0] is letter}
    stat_total = 0

    for key, value in subdict.items():
        stat_total += value
    return subdict, stat_total

def gen_word(min=2, max=100):
    word = weighted_random(*gen_subdict(collocation_stat, ' '))

    while (word[-1] != ' ' or len(word.strip()) < min) and len(word) < max :
        word = word.strip()
        next_letter = weighted_random(*gen_subdict(collocation_stat, word[-1]))
        word += next_letter[1]

    return word.strip()

if __name__ == '__main__':
    print("Press enter to generate word; type anything to exit.")

    gen_lower_limit = 3
    gen_upper_limit = 12

    while not input():
        word = gen_word(min = gen_lower_limit, max = gen_upper_limit)
        print(word, end='')
