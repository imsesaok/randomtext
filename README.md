# RandomText

RandomText is a project aiming to generate 'pronounceable' random strings.

# How to use

> python randomtext.py

will run a interactive dialog for generating words. By default, word length is limited from 3 to 12.

Before you can use RandomText to generate string, you need to create a `collocation-stat.txt` file, which can be generated with a list of words and `prob-extract.py`.

To generate `collocation-stat.txt`, execute:

> python prob-extract.py path-to-raw-word-list

`prob-extract.py` assumes the input file contains one word per line without any extra letters, including punctuations. Specifically, ensure your data does not contain any comma(,) as it is used as a delimiter and any extra comma will break the `randomtext.py` script.
