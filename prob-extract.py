import sys

collocation_stat=dict()

def increment(stat_dict, key):
    if key in stat_dict:
        stat_dict[key] = stat_dict[key]+1
    else:
        stat_dict[key] = 1

def gen_stat(word):
    word = word.strip()
    for i in range(len(word)):
        a = word[i]
        b = word[i+1] if i < len(word) - 1 else ' '
        
        increment(collocation_stat, a+b)
        if i is 0:
            increment(collocation_stat, ' '+a)

def export(stat_dict, filename):
    with open(filename, "w") as stat_file:
        for key, value in stat_dict.items():
            stat_file.write(str(key)+","+str(value)+"\n")

if __name__ == "__main__":
    try:
        with open(sys.argv[1]) as word_file:
            for word in word_file:
                gen_stat(word)
    except IndexError:
        print("Usage: python prob-extract.py <path to raw word list>")

    export(collocation_stat, "collocation_stat.txt")

